# Install unzip (we'll need it)
apt-get update -qq
apt-get install -qq -y unzip
apt-get install -qq -y wget

# Install terraform (we need the specific version)
wget --no-check-certificate https://releases.hashicorp.com/terraform/0.12.5/terraform_0.12.5_linux_amd64.zip
unzip -o ./terraform_0.12.5_linux_amd64.zip
mv -f terraform /usr/local/bin/