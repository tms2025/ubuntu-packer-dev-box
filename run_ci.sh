echo "Saving private key from secrets"
mkdir ~/.ssh
# The newlines don't store into environment variables properly, so store then as splats then replace them when gen'ing the key.
echo "$PRIVATE_KEY" | tr "*" "\n" > ~/.ssh/id_rsa
chmod 0600 ~/.ssh/id_rsa

echo "Installing apt dependencies"
apt-get update -qq
apt-get install -y -qq ssh

echo "initialize terraform so we can get output"
cd ./terraform-config/
echo "$GCP_KEY" > ./ubuntu-packer.json
terraform init

echo "Extracting IP"
instance_ip=$(terraform output ip_address)
echo "IP Extracted: $instance_ip"

echo "Sleep to wait for SSH to be available"
sleep 1m

if [ ! -z "$instance_ip" ]
then
  echo "Attempting ssh"
  scp  -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no ../run_packer.sh ubuntu@$instance_ip:~/run_packer.sh
  version=$(date +"%Y.%m.%d.%H.%M")
  echo "Starting Build for $CI_COMMIT_REF_NAME"
  ssh ubuntu@$instance_ip -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa "chmod +x ~/run_packer.sh && ~/run_packer.sh $version $VAGRANT_CLOUD_TOKEN $CI_COMMIT_REF_NAME"
else
  echo "got here with no IP. That's bad. Exiting"
  exit -1
fi