resource "google_compute_network" "network" {
  name = "${var.network}"
}

resource "google_compute_subnetwork" "subnet" {
  name          = "${var.network}-subnet-${var.region}"
  region        = "${var.region}"
  network       = "${google_compute_network.network.self_link}"
  ip_cidr_range = "10.0.0.0/16"
}

resource "google_compute_firewall" "ssh" {
  name    = "${var.network}-firewall-ssh"
  network = "${google_compute_network.network.name}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  target_tags   = ["${var.network}-firewall-ssh"]
  source_ranges = ["0.0.0.0/0"]
}
