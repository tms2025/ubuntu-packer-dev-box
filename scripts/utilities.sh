#!/bin/bash -eux

export DEBIAN_FRONTEND=noninteractive

# Change to tmp to prevent issues from executing commands in a non-existance folder
cd /tmp

# install ansible
apt-get update -qq -y
apt-get install -qq -y software-properties-common
apt-add-repository --yes --update ppa:ansible/ansible
apt-get install -qq -y ansible

# Cleanup terraform install
rm -rf /tmp/terraform

# Install awscli
apt-get install -qq -y awscli

# Install gradle
apt-get install -qq -y gradle

# Install NodeJS & NPM
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
apt-get install -y nodejs build-essential

# Install gitkraken
mkdir -p /tmp/gitkraken
cd /tmp/gitkraken

wget -q https://release.gitkraken.com/linux/gitkraken-amd64.deb

# Run fixBroken on apt, because that's required here for some reason
apt-get -y --fix-broken install
apt install -qq -y gconf2

# run the package install
dpkg -i ./gitkraken-amd64.deb

# Cleanup gitkraken install
cd /tmp
rm -rf /tmp/gitkraken

# install docker
apt-get install -qq -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# install docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod 0755 /usr/local/bin/docker-compose
	
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-get update -qq -y
apt install -qq -y docker.io

# Add the vagrant user to the "docker" group so they can run commands without sudo
usermod -a -G docker vagrant

mkdir /tmp/jmeter
cd /tmp/jmeter

wget -q https://downloads.apache.org//jmeter/binaries/apache-jmeter-5.3.tgz
mkdir -p /home/vagrant/jmeter
tar -xzf ./apache-jmeter-5.3.tgz -C /home/vagrant/jmeter --strip-components=1

cd /home/vagrant/jmeter/lib/ext
wget -q -O ./plugin-manager.jar https://repo1.maven.org/maven2/kg/apc/jmeter-plugins-manager/1.4/jmeter-plugins-manager-1.4.jar

chown -R vagrant/vagrant /home/vagrant/jmeter
cd /tmp
rm -rf /tmp/jmeter

echo "[Desktop Entry]
Type=Application
Name=JMeter
Comment=JMeter
Icon=/home/vagrant/jmeter/docs/images/jmeter.png
Exec=sudo -k -u root /home/vagrant/jmeter/bin/jmeter
Terminal=false
Categories=Development;IDE;Jmeter;" > /usr/share/applications/jmeter.desktop


# Install Postman
mkdir /tmp/postman
cd /tmp/postman

# Install libconf in case it isn't installed
apt install -qq -y libgconf-2-4

wget -q https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz
mkdir -p /home/vagrant/
tar -xzf postman.tar.gz -C /home/vagrant/
ln -s /home/vagrant/Postman/Postman /usr/bin/postman
rm postman.tar.gz

chown -R vagrant/vagrant /home/vagrant/Postman

# For whatever reason, postman's install oscillates between having the icon in app/resources/app and just having it in resources/app. So we need to find it first.
export ICON_LOC=`find /opt/Postman/ -name icon.png`

echo "[Desktop Entry]
Type=Application
Name=Postman
Comment=Spring Tool Suite
Icon=${ICON_LOC}
Exec=postman
Terminal=false
Categories=Development;IDE;Java;" > /usr/share/applications/postman.desktop

# Install Gauge
mkdir /tmp/gauge
cd /tmp/gauge

wget https://github.com/getgauge/gauge/releases/download/v1.0.6/gauge-1.0.6-linux.x86_64.zip
unzip ./gauge-1.0.6-linux.x86_64.zip -d /usr/bin/
chown vagrant/vagrant /usr/bin/gauge

cd /tmp
rm -rf /tmp/gauge

# Remove a bunch of unnecessary favorites from the sidebar
sudo -Hu vagrant dbus-launch gsettings set org.gnome.shell favorite-apps "['firefox.desktop']"
